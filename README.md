### Configuration

Install dependencies using

```sh
$ npm install
```


Create a sample database using following commands

```sh
$ node db.js
```
optinally you can pass parameters to the above command. see `node db.js --help` for more info.

To delete a sample database

```sh
$ node db.js --delete
```

Note: Default name of the database is `userView` and name of the collection is `userView`.

### Usage

Find total users and total unique users

```sh
$ node app.js
```

Find users who viewed a product today

```sh
$ node app.js -n 1
```

Find users who viewed a product in last 7 days

```sh
$ node app.js -n 7
```

Find users who viewed a product between two specified dates

```sh
$ node app.js -f 01/02/2020 -t 01/06/2020-14:22:30
```

for more info, see `node app.js --help`.

- Above date is in MM/DD/YYYY format.
- Passing option `-n` will ignore `-f` and `-t`.
- If no options are specified, then the search will return today's result.
