const MongoClient = require('mongodb').MongoClient;
const client = new MongoClient('mongodb://localhost:27017', {
  useUnifiedTopology: true
});

const argv = require('yargs')
  .usage('Usage: $0 [-n <number>] [-f <date>] [-t <date>]')
  .options({
    'n': {
      type: 'number',
      description: 'Number of days'
    },
    'f': {
      alias: 'from',
      type: 'string',
      description: 'Start date'
    },
    't': {
      alias: 'to',
      type: 'string',
      description: 'End date'
    }
  })
  .coerce(['f', 't'], input => new Date(input))
  .argv;

let dateFrom, dateTo;

// ignore -f and -t in -n is passed
if (argv.n || argv.n === 0) {
  dateTo = new Date();
  dateFrom = dateWithoutTime(dateTo, argv.n - 1);
}

dateTo = dateTo || argv.to || new Date();
dateFrom = dateFrom || argv.from || dateWithoutTime(dateTo);

if (dateFrom > dateTo) {
  console.error('start date must be less than end date');
  process.exit(1);
}

console.log('From: ' + dateFrom.toISOString());
console.log('To: ' + dateTo.toISOString());

(async function () {

  try {
    await client.connect();
  } catch (err) {
    throw "Database connection failed";
  }

  const db = client.db('userView');
  const collection = db.collection('userView');

  // Find total users between given dates
  const totalUsersPromise = collection.find({ ViewDate: { $gte: dateFrom, $lt: dateTo } }).count();

  // Find total unique users between given dates
  const totalUniqueUserPromise = collection.aggregate([
    { $match: { ViewDate: { $gte: dateFrom, $lt: dateTo } } },
    { $group: { _id: "$UserId" } },
    { $count: "count" }
  ]).toArray();

  try {
    const response = await Promise.all([totalUsersPromise, totalUniqueUserPromise]);
    console.log('Total Users: ' + response[0]);
    console.log('Total Unique Users: ' + response[1][0].count);
  } catch (err) {
    console.log(err);
  }

  await client.close();
})();

/**
 * Remove time from a date.
 *
 * @param date      Original date
 * @param skipDays  Number of days to remove from the current date
 * @returns {Date}
 */
function dateWithoutTime(date, skipDays = 0) {
  let newDate = new Date(date.getTime() - skipDays * 24 * 60 * 60 * 1000);
  newDate.setHours(0, 0, 0, 0);
  return newDate;
}
