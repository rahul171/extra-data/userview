const MongoClient = require('mongodb').MongoClient;
const client = new MongoClient('mongodb://localhost:27017', {
  useUnifiedTopology: true
});

const argv = require('yargs')
  .usage('Usage: $0 [-u <number>] [-p <number>] [-d <number>] [-r <number>] [--delete]')
  .options({
    'users': {
      alias: 'u',
      type: 'number',
      description: 'Number of users to create'
    },
    'products': {
      alias: 'p',
      type: 'number',
      description: 'Number of products to create'
    },
    'dates': {
      alias: 'd',
      type: 'number',
      description: 'Number of dates to create'
    },
    'range': {
      alias: 'r',
      type: 'number',
      description: 'Maximum number of products per user'
    },
    'delete': {
      type: 'boolean',
      description: 'Delete the database'
    }
  })
  .argv;

const un = argv.u || 1000000,
  pn = argv.p || 20,
  dn = argv.d || 30,

  // should be less than pn
  pr = argv.r || 5;

(async function () {

  try {
    await client.connect();
  } catch (err) {
    throw "Database connection failed";
  }

  const db = client.db('userView');
  const collection = db.collection('userView');

  if (argv.delete) {
    try {
      await db.dropDatabase();
      console.log('Database deleted successfully');
      await client.close();
    } catch (err) {
      console.log(err);
    }
    return;
  }

  let data = [];

  console.log('Creating data...');

  for (let i = 1; i <= un; i++) {
    const random = generateRandomNumber(1, pn - pr);
    for (let j = random; j <= random + generateRandomNumber(1, pr); j++) {
      const date = new Date(Date.now() - generateRandomNumber(0, dn - 1) * 24 * 60 * 60 * 1000);
      data.push({
        UserId: 'user' + i,
        ViewDate: date,
        ProductId: 'product' + j
      });
    }
  }

  try {
    console.log('Inserting data into database...');
    await collection.insertMany(data);
    console.log('Data inserted successfully');
    await client.close();
  } catch (err) {
    console.log(err);
  }
})();

/**
 * Generate random number between two numbers (inclusive)
 *
 * @param a Start number
 * @param b End number
 * @returns {number} Generated random number
 */
function generateRandomNumber(a = 0, b = 1) {
  return a + Math.floor((b - a + 1) * Math.random());
}
